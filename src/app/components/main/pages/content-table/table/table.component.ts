import { AfterViewInit, ChangeDetectorRef, Component, ComponentFactoryResolver, ComponentRef, EventEmitter, Input, Output, ViewChild, ViewContainerRef } from '@angular/core';
import { ContentLine } from 'src/app/models/content-line';
import { Content } from 'src/app/models/content';
import { Doc } from 'src/app/models/document';
import { SectionComponent } from '../section/section.component';

@Component({
  selector: 'app-content-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements AfterViewInit {

  @Input() document: Doc;
  @Input() content: Content[];
  @Input() marge: any;
  @Input() isEdit: boolean;
  @Output() needNewPageEvent = new EventEmitter<Content[]>();
  @ViewChild('table', {static: true, read: ViewContainerRef}) table: ViewContainerRef;

  private sectionInstance: ComponentRef<SectionComponent> = null;
  private eventEmitted: boolean = false;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private cdr: ChangeDetectorRef
  ) { }

  ngAfterViewInit(): void {
    var tempContent = [...this.content];
    for (let section of this.content) {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(SectionComponent);
      this.sectionInstance = this.table.createComponent(componentFactory);
      this.sectionInstance.instance.section = section;
      this.sectionInstance.instance.marge = this.marge;
      this.sectionInstance.instance.isEdit = this.isEdit;
      var componentElement = this.sectionInstance.location.nativeElement;
      const sibling: HTMLElement = componentElement.previousSibling;
      sibling.appendChild(componentElement);

      // Article Line overflowing
      this.sectionInstance.instance.endSectionEvent.subscribe((data: ContentLine[]) => {
        if (section.content.length == data.length) {
          sibling.removeChild(componentElement);
        } else {
          var tempContentBis: Content = JSON.parse(JSON.stringify(tempContent[0]));
          tempContent.shift();
          tempContentBis.displayName = false;
          tempContentBis.content = data;
          tempContent.unshift(tempContentBis);
        }
        this.eventEmitted = true;
        this.needNewPageEvent.emit(tempContent);
      });
      this.cdr.detectChanges();
      
      if (this.eventEmitted) {
        return;
      } else {
        tempContent.shift();
      }
    }
  }
}
