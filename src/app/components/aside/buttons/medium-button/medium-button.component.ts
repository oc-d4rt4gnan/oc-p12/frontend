import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-medium-button',
  templateUrl: './medium-button.component.html',
  styleUrls: ['./medium-button.component.scss']
})
export class MediumButtonComponent implements OnInit {

  @Input() displayedText: string;
  @Input() class: string;
  buttonIcon: string;

  constructor() { }

  ngOnInit(): void {
    this.buttonIcon = this.initIcon();
  }

  initIcon(): string {
    switch (this.class) {
      case 'return-button':
        return 'icon-left-arrow';
      case 'new-button':
        return 'icon-plus';
      case 'delete-button':
        return 'icon-trash';
      case 'print-button':
        return 'icon-print';
      case 'download-button':
        return 'icon-pdf';
      case 'edit-button':
        return 'icon-edit';
      case 'cancel-button':
        return 'icon-cross';
      case 'save-button':
      return 'icon-save';
      default:
        return '';
    }
  }

}
