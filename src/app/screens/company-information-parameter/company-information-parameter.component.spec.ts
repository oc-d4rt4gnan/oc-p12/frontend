import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyInformationParameterComponent } from './company-information-parameter.component';

describe('CompanyInformationParameterComponent', () => {
  let component: CompanyInformationParameterComponent;
  let fixture: ComponentFixture<CompanyInformationParameterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyInformationParameterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyInformationParameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
