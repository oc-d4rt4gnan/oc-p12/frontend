import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CircleToRhumbusesSpinnerComponent } from './circle-to-rhumbuses-spinner.component';

describe('CircleToRhumbusesSpinnerComponent', () => {
  let component: CircleToRhumbusesSpinnerComponent;
  let fixture: ComponentFixture<CircleToRhumbusesSpinnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CircleToRhumbusesSpinnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CircleToRhumbusesSpinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
