import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentInformationParameterComponent } from './document-information-parameter.component';

describe('DocumentInformationParameterComponent', () => {
  let component: DocumentInformationParameterComponent;
  let fixture: ComponentFixture<DocumentInformationParameterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocumentInformationParameterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentInformationParameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
