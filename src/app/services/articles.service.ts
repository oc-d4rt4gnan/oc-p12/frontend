import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ContentLine } from '../models/content-line';
import { ArticleMapperService } from '../mappers/article-mapper.service';
import { ItemsService } from '../core/api/v1/api/items.service';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  subjectSearchNotifier: Subject<any> = new Subject<any>();

  constructor(private http: HttpClient, private articlesService: ItemsService, private articlesMapper: ArticleMapperService) { }

  getArticles = (keyword?: string): Observable<ContentLine[]> => {
    let subjectArticles:Subject<ContentLine[]> = new Subject();
    this.articlesService.getItems(keyword, 20).subscribe(articles => {
      var localArticles: ContentLine[] = this.articlesMapper.toContentLineArray(articles);
      subjectArticles.next(localArticles);
      subjectArticles.complete();
    });
    return subjectArticles;
  }

  getArticlesName = (articles: ContentLine[]): string[] => {
    return articles.map(article => article.designation);
  }

  notifyAboutSearch = (articlesName: ContentLine[]): void => {
    this.subjectSearchNotifier.next(articlesName);
  }
}
