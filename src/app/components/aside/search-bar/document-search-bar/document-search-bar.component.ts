import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DocumentService } from 'src/app/services/document.service';

@Component({
  selector: 'document-search-bar',
  templateUrl: './document-search-bar.component.html',
  styleUrls: ['./document-search-bar.component.scss']
})
export class DocumentSearchBarComponent implements OnInit {

  keyword: string = '';

  @ViewChild('searchBar') searchBar: ElementRef;

  constructor(private documentService: DocumentService, private router: Router) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    if (this.router.parseUrl(this.router.url).queryParamMap.has('keyword')) {
      this.searchBar.nativeElement.value = this.router.parseUrl(this.router.url).queryParamMap.get('keyword');
    }
  }

  onSearch = (): void => {
    this.keyword = this.searchBar.nativeElement.value;
    this.navigate();
  }

  navigate = (): void => {
    this.router.navigateByUrl(this.setUrl()).then(() => this.afterNavigate());
  }

  private afterNavigate = (): void => {
    var page = parseInt(this.router.parseUrl(this.router.url).queryParamMap.get('page'));
    var year = parseInt(this.router.parseUrl(this.router.url).queryParamMap.get('year'));
    var keyword = this.router.parseUrl(this.router.url).queryParamMap.get('keyword');
    this.documentService.notifyAboutPageChange(page, year, keyword);
  }

  private setUrl = (): string => {
    var url = this.router.url;

    if (this.router.parseUrl(this.router.url).queryParamMap.has('page')) {
      url = url.replace('page='+this.router.parseUrl(this.router.url).queryParamMap.get('page'), 'page=1');
    } else {
      if (url.includes('?')) {
        url = url + '&page=1';
      } else {
        url = url + '?page=1';
      }
    }

    if (!this.router.parseUrl(this.router.url).queryParamMap.has('year')) {
      var year = new Date().getFullYear();
      if (url.includes('?')) {
        url = url + '&year=' + year;
      } else {
        url = url + '?year=' + year;
      }
    }

    if (this.router.parseUrl(this.router.url).queryParamMap.has('keyword')) {
      url = url.replace('keyword='+this.router.parseUrl(this.router.url).queryParamMap.get('keyword'), 'keyword=' + this.keyword);
    } else {
      if (url.includes('?')) {
        url = url + '&keyword=' + this.keyword;
      } else {
        url = url + '?keyword=' + this.keyword;
      }
    }

    return url;
  }

}
