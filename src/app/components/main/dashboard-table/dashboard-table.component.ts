import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Type } from 'src/app/core/api/v1/model/type';
import { Doc } from 'src/app/models/document';
import { DocumentService } from 'src/app/services/document.service';

@Component({
  selector: 'app-dashboard-table',
  templateUrl: './dashboard-table.component.html',
  styleUrls: ['./dashboard-table.component.scss']
})
export class DashboardTableComponent implements OnInit {

  documents: Doc[];
  statusTitle: string;
  type: Type;
  isDocumentsEmpty: boolean = true;

  constructor(private router: Router, private documentService: DocumentService) {
    switch (true) {
      case this.isUrlContains('/quotation'):
        this.statusTitle = "Accepté";
        this.type = Type.Quotation;
        break;
      case this.isUrlContains('/invoice'):
        this.statusTitle = "Payée";
        this.type = Type.Invoice;
        break;
      default:
        this.statusTitle = "";
        this.type = Type.Undefined;
        break;
    }
    var urlPageNumber: string = this.router.parseUrl(this.router.url).queryParamMap.get("page");
    var pageNumber: number = urlPageNumber != null ? parseInt(urlPageNumber) : 1;
    var urlYear: string = this.router.parseUrl(this.router.url).queryParamMap.get("year");
    var year: number = urlYear != null ? parseInt(urlYear) : new Date().getFullYear();
    var urlKeyword: string = this.router.parseUrl(this.router.url).queryParamMap.get("keyword");
    var keyword: string = urlKeyword != null ? urlKeyword : '';
    this.fetchData(pageNumber, year, keyword);
  }

  ngOnInit(): void {
  }
  
  notifierPageChangeSubscription: Subscription = this.documentService.subjectPageChangeNotifier.subscribe(data => {
    var pageNumber: number = data.page;
    var year: number = data.year;
    var keyword: string = data.keyword;
    this.fetchData(pageNumber, year, keyword);
  });

  private isUrlContains = (path:string) : boolean => {
    return this.router.url.includes(path);
  }

  private fetchData = (pageNumber: number, year: number, keyword: string): void => {
    this.documentService.getDocuments(this.type, pageNumber, year, keyword).subscribe(documentArray => {
      this.documents = documentArray;
      this.isDocumentsEmpty = documentArray == undefined || documentArray.length == 0;
    });
  }

}
