import { TestBed } from '@angular/core/testing';

import { CompanyInformationMapperService } from './company-information-mapper.service';

describe('CompanyInformationMapperService', () => {
  let service: CompanyInformationMapperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CompanyInformationMapperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
