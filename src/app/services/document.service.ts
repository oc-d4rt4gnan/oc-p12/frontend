import { ElementRef, Injectable } from '@angular/core';
import { jsPDF } from 'jspdf'
import html2canvas from 'html2canvas';
import { Observable, Subject } from 'rxjs';
import { Doc } from 'src/app/models/document';
import { Document } from 'src/app/core/api/v1/model/document';
import { Type } from 'src/app/core/api/v1/model/type';
import { Canvas } from 'src/app/models/canvas';
import { HttpClient } from '@angular/common/http';
import { ContentLine } from '../models/content-line';
import { DocumentsService } from "src/app/core/api/v1/api/documents.service";
import { DocumentMapperService } from "src/app/mappers/document-mapper.service";

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  subjectDeletionNotifier: Subject<any> = new Subject<any>();
  subjectAddingNotifier: Subject<any> = new Subject<any>();
  subjectTotalUpdateNotifier: Subject<any> = new Subject<any>();
  subjectSelectionNotifier: Subject<any> = new Subject<any>();
  subjectPushArticleInSelectedLineNotifier: Subject<any> = new Subject<any>();
  subjectTotalPageNotifier: Subject<number> = new Subject<number>();
  subjectPageChangeNotifier: Subject<any> = new Subject<any>();

  hasPermission: boolean = true;

  constructor(
    private http: HttpClient, 
    private documentsService: DocumentsService,
    private documentMapper: DocumentMapperService) { }

  notifyAboutDeletion = (element: any): void => {
    this.subjectDeletionNotifier.next(element);
  }

  notifyAboutAdding = (element: any): void => {
    this.subjectAddingNotifier.next(element);
  }

  notifyAboutTotalUpdate = (): void => {
    this.subjectTotalUpdateNotifier.next();
  }

  notifyAboutTotalUpdateBis() {
    throw new Error('Method not implemented.');
  }

  notifyAboutSelection = (): void => {
    this.subjectSelectionNotifier.next();
  }

  notifyAboutPushArticleInSelectedLine = (article: ContentLine): void => {
    this.subjectPushArticleInSelectedLineNotifier.next(article);
  }

  notifyAboutPageChange = (page?: number, year?: number, keyword?: string): void => {
    this.subjectPageChangeNotifier.next({page: page, year: year, keyword: keyword});
  }

  getDocuments = (type: Type, pageNumber: number, year: number, searchWord?: string): Observable<Doc[]> => {
    pageNumber = pageNumber != null ? pageNumber : 1;
    year = year != null ? year : new Date().getFullYear();
    let subjectLocalDocuments:Subject<Doc[]> = new Subject();
    this.documentsService.getDocuments(type, pageNumber, year, searchWord, 'response').subscribe(response => {
      this.subjectTotalPageNotifier.next(parseInt(response.headers.get("X-Total-Page")));
      var localDocumentArray: Doc[] = this.documentMapper.toLocalDocuments(response.body);
      subjectLocalDocuments.next(localDocumentArray);
      subjectLocalDocuments.complete();
    });
    return subjectLocalDocuments;
  }

  getDocumentDetails = (reference: string, type: Type): Observable<Doc> => {
    let subjectLocalDocument:Subject<Doc> = new Subject();
    this.documentsService.getDocument(reference, type).subscribe(document => {
      var localDocument: Doc = this.documentMapper.toLocalDocument(document);
      subjectLocalDocument.next(localDocument);
      subjectLocalDocument.complete();
    });
    return subjectLocalDocument;
  }

  getNewDocumentPattern = (type: Type): Observable<Doc> => {
    let subjectLocalDocuments:Subject<Doc> = new Subject();
    this.documentsService.getNewDocument(type).subscribe(document => {
      var localDocument: Doc = this.documentMapper.toLocalDocument(document);
      subjectLocalDocuments.next(localDocument);
      subjectLocalDocuments.complete();
    });
    return subjectLocalDocuments;
  }

  save = (doc: Doc): Observable<Doc> => {
    let subjectLocalDocument:Subject<Doc> = new Subject();
    var document: Document = this.documentMapper.toApiDocument(doc);
    this.documentsService.addDocument(document).subscribe(document => {
      var localDocument: Doc = this.documentMapper.toLocalDocument(document);
      subjectLocalDocument.next(localDocument);
      subjectLocalDocument.complete();
    });
    return subjectLocalDocument;
  }

  downloadDocumentAsPDF = async (document: ElementRef, url: string) => {
    var PDF = await this.setPDF(document);
    PDF.save(this.constructPDFName(url));
  }

  printDocumentAsPDF = async (document: ElementRef) => {
    var PDF = await this.setPDF(document);
    PDF.autoPrint();
    window.open(PDF.output('bloburl'), '_blank');
  }

  private setPDF = async (document: ElementRef): Promise<jsPDF> => {
    const pages = document.nativeElement.children[0].children[0];
    let PDF = new jsPDF('p', 'mm', 'a4');
    var pageNumber = 1;
    const canvasList = await this.getCanvasList(pages);
    for (let canvas of canvasList) {
      if (pageNumber > 1) {
        PDF.addPage('a4', 'p');
        PDF.setPage(pageNumber);
      }
      PDF.addImage(canvas.uri, 'PNG', 0, 0, canvas.width, canvas.height);
      pageNumber++;
    }
    return PDF;
  }

  private constructPDFName = (url: string): string => {
    var urlArray = url.split('/');
    var reference = urlArray.pop().split('?')[0];
    var type = url.includes('/quotation') ? 'devis' : 'facture';
    return type + '_' + reference + '.pdf';
  }

  private getCanvasList = (pages: any): Promise<Canvas[]> => {
    return new Promise(async (resolve) => {
      var canvasArray: Canvas[] = new Array<Canvas>();
      for (let page of pages.children) {
        const canvas = await this.getCanvas(page);
        canvasArray.push(canvas);
      }
      resolve(canvasArray);
    });
  }

  private getCanvas = (page: any): Promise<Canvas> => {
    return new Promise((resolve) => {
      page.children[0].classList.add('print');
      html2canvas(page).then((canvas) => {
        let newCanvas = new Canvas();
        newCanvas.width = 210;
        newCanvas.height = (canvas.height * newCanvas.width) / canvas.width;
        newCanvas.uri = canvas.toDataURL('image/png', 100.0);
        page.children[0].classList.remove('print');
        resolve(newCanvas);
      });
    });
  }

}

