import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-parameter-button',
  templateUrl: './parameter-button.component.html',
  styleUrls: ['./parameter-button.component.scss']
})
export class ParameterButtonComponent implements OnInit {

  @Input() displayedText: string;
  @Input() icon: string;

  constructor() { }

  ngOnInit(): void {
  }

}
