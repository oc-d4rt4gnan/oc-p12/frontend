import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalInformationParameterComponent } from './personal-information-parameter.component';

describe('PersonalInformationParameterComponent', () => {
  let component: PersonalInformationParameterComponent;
  let fixture: ComponentFixture<PersonalInformationParameterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalInformationParameterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalInformationParameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
