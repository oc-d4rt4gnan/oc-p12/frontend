import { Injectable } from '@angular/core';
import { Company } from '../core/api/v1/model/company';
import { PersonalInformation } from '../models/personal-information';
import { ApiCompany } from '../models/api-impl/api-company';

@Injectable({
  providedIn: 'root'
})
export class PersonalInformationMapperService {

  constructor() { }

  toPersonalInformation = (company: Company): PersonalInformation => {
    var personalInformation: PersonalInformation = new PersonalInformation();

    personalInformation.companyId = company.id != null ? company.id : '';
    personalInformation.address = company.address != null ? company.address : '';
    personalInformation.email = company.email != null ? company.email : '';
    personalInformation.firstName = company.firstName != null ? company.firstName : '';
    personalInformation.lastName = company.lastName != null ? company.lastName : '';
    personalInformation.phoneNumber = company.telephone != null ? company.telephone : '';

    return personalInformation;
  }

  toCompany = (personalInformation: PersonalInformation): Company => {
    var company: Company = new ApiCompany();

    company.id = personalInformation.companyId;
    company.address = personalInformation.address;
    company.email = personalInformation.email
    company.firstName = personalInformation.firstName;
    company.lastName = personalInformation.lastName;
    company.telephone = personalInformation.phoneNumber;

    return company;
  }
}
