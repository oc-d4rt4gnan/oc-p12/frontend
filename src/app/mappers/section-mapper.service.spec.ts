import { TestBed } from '@angular/core/testing';

import { SectionMapperService } from './section-mapper.service';

describe('SectionMapperService', () => {
  let service: SectionMapperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SectionMapperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
