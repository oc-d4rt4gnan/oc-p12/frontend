import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-pagination-button',
  templateUrl: './pagination-button.component.html',
  styleUrls: ['./pagination-button.component.scss']
})
export class PaginationButtonComponent implements OnInit {

  @Input() content: string = "";
  @Input() id: string = "";
  
  constructor() { }

  ngOnInit(): void {
    this.id = "page"+this.id;
  }

}
