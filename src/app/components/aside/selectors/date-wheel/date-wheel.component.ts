import { Component, ElementRef, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { DocumentService } from 'src/app/services/document.service';

@Component({
  selector: 'app-date-wheel',
  templateUrl: './date-wheel.component.html',
  styleUrls: ['./date-wheel.component.scss']
})
export class DateWheelComponent implements OnInit {

  @Output() selectedDate: number;
  wheel: any;
  years = [];
  currentParam: string;

  constructor(private elRef: ElementRef, private router: Router, private documentService: DocumentService) { 
    for(let i = new Date().getFullYear(); i >= 2000; i--) { 
      this.years.push(i);
    }
    if (this.router.parseUrl(this.router.url).queryParamMap.has('year')) {
      var year: number = parseInt(this.router.parseUrl(this.router.url).queryParamMap.get('year'));
      var spliced: Array<number> = this.years.splice(0, this.years.indexOf(year));
      this.years = this.years.concat(spliced);
    }
  }

  ngOnInit(): void {
    this.wheel = this.elRef.nativeElement.querySelector('#wheel');
    this.currentParam = this.router.parseUrl(this.router.url).queryParamMap.get('year');
  }

  ngAfterViewInit(): void {
    this.setWheel();
  }

  onElementScroll = ($event: any) => {
    this.isScrollUp($event) ? this.scroll() : this.unscroll();
    this.setWheel();
    this.navigate();
  }

  setWheel = () => {
    var listElements = this.wheel.getElementsByTagName("li");

    var step = (2*Math.PI)/listElements.length;
    var angle = 0; 
    var circleCenterX = 0;
    var circleCenterY = this.wheel.offsetHeight/2;
    var radius = this.wheel.offsetWidth-(this.wheel.offsetWidth*0.4);

    for(var i = 0; i < listElements.length; i++) { 
      var element = listElements[i];
      var paragraph = element.getElementsByTagName("p")[0];
      var left = Math.round(circleCenterX+radius*Math.cos(angle));
      var top = Math.round(circleCenterY+radius*Math.sin(angle));

      if (i == 0) { 
        element.classList.add('selected-date');
        this.selectedDate = parseInt(paragraph.innerHTML);
      } else { 
        element.classList.remove('selected-date');
      }
      element.style.left = left+"px";
      element.style.top = top+"px";
      paragraph.style.transform = "rotate("+360*i/listElements.length+"deg)";
      
      angle+=step;   
    }
  }

  scroll = () => {
    const items = this.wheel.querySelector("ul");
    items.prepend(items.lastChild);
  }
   
  unscroll = () => {
    const items = this.wheel.querySelector("ul");
    items.appendChild(items.firstChild);
  }

  isScrollUp = ($event: { deltaY: number; }) => {
    return $event.deltaY < 0;
  }

  navigate = (): void => {
    this.router.navigateByUrl(this.setUrl()).then(() => this.afterNavigate());
  }

  private afterNavigate = (): void => {
    this.currentParam = ''+this.selectedDate;
    var page = parseInt(this.router.parseUrl(this.router.url).queryParamMap.get('page'));
    var year = parseInt(this.router.parseUrl(this.router.url).queryParamMap.get('year'));
    var keyword = this.router.parseUrl(this.router.url).queryParamMap.get('keyword');
    this.documentService.notifyAboutPageChange(page, year, keyword);
  }

  private setUrl = (): string => {
    var url = this.router.url;

    if (this.router.parseUrl(this.router.url).queryParamMap.has('page')) {
      url = url.replace('page='+this.router.parseUrl(this.router.url).queryParamMap.get('page'), 'page=1');
    } else {
      if (url.includes('?')) {
        url = url + '&page=1';
      } else {
        url = url + '?page=1';
      }
    }

    if (this.router.parseUrl(this.router.url).queryParamMap.has('year')) {
      url = url.replace('year='+this.router.parseUrl(this.router.url).queryParamMap.get('year'), 'year=' + this.selectedDate);
    } else {
      if (url.includes('?')) {
        url = url + '&year=' + this.selectedDate;
      } else {
        url = url + '?year=' + this.selectedDate;
      }
    }

    if (!this.router.parseUrl(this.router.url).queryParamMap.has('keyword')) {
      if (url.includes('?')) {
        url = url + '&keyword=';
      } else {
        url = url + '?keyword=';
      }
    }

    return url;
  }

}
