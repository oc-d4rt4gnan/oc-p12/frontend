import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './screens/dashboard/dashboard.component';
import { HomeComponent } from './screens/home/home.component';
import { DetailsComponent } from './screens/details/details.component';
import { ParametersComponent } from './screens/parameters/parameters.component';
import { PersonalInformationParameterComponent } from './screens/personal-information-parameter/personal-information-parameter.component';
import { CompanyInformationParameterComponent } from './screens/company-information-parameter/company-information-parameter.component';
import { DocumentInformationParameterComponent } from './screens/document-information-parameter/document-information-parameter.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'invoice', component: DashboardComponent },
  { path: 'quotation', component: DashboardComponent },
  { path: 'quotation/read/:document.reference', component: DetailsComponent },
  { path: 'invoice/read/:document.reference', component: DetailsComponent },
  { path: 'quotation/edit/:document.reference', component: DetailsComponent },
  { path: 'invoice/edit/:document.reference', component: DetailsComponent },
  { path: 'quotation/edit/new-document', component: DetailsComponent },
  { path: 'invoice/edit/new-document', component: DetailsComponent },
  { path: 'parameters', component: ParametersComponent },
  { path: 'parameters/personal-information', component: PersonalInformationParameterComponent },
  { path: 'parameters/company-information', component: CompanyInformationParameterComponent },
  { path: 'parameters/document-information', component: DocumentInformationParameterComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
