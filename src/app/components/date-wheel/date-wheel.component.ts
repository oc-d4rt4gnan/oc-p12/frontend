import { Component, ElementRef, OnInit, Output, ViewChild, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'app-date-wheel',
  templateUrl: './date-wheel.component.html',
  styleUrls: ['./date-wheel.component.scss']
})
export class DateWheelComponent implements OnInit {

  @Output() selectedDate: number;
  wheel: any;

  years = [];

  constructor(private elRef:ElementRef) { 
    for(let i = 2000; i <= 2022; i++) { 
      this.years.push(i);
    }
  }

  ngOnInit(): void {
    this.wheel = this.elRef.nativeElement.querySelector('#wheel');
  }

  ngAfterViewInit(): void {
    this.setWheel();
  }

  onElementScroll = ($event: any) => {
    this.isScrollUp($event) ? this.scroll() : this.unscroll();
    this.setWheel();
  }

  setWheel = () => {
    var listElements = this.wheel.getElementsByTagName("li");

    var step = (2*Math.PI)/listElements.length;
    var angle = 0; 
    var circleCenterX = 0;
    var circleCenterY = this.wheel.offsetHeight/2;
    var radius = this.wheel.offsetWidth-(this.wheel.offsetWidth*0.4);

    for(var i = 0; i < listElements.length; i++) { 
      var element = listElements[i];
      var paragraph = element.getElementsByTagName("p")[0];
      var left = Math.round(circleCenterX+radius*Math.cos(angle));
      var top = Math.round(circleCenterY+radius*Math.sin(angle));

      if (i == 0) { 
        element.classList.add('selected-date');
        this.selectedDate = parseInt(paragraph.innerHTML);
      } else { 
        element.classList.remove('selected-date');
      }
      element.style.left = left+"px";
      element.style.top = top+"px";
      paragraph.style.transform = "rotate("+360*i/listElements.length+"deg)";
      
      angle+=step;   
    }
  }

  scroll = () => {
    const items = this.wheel.querySelector("ul");
    items.prepend(items.lastChild);
  }
   
  unscroll = () => {
    const items = this.wheel.querySelector("ul");
    items.appendChild(items.firstChild);
  }

  isScrollUp = ($event: { deltaY: number; }) => {
    return $event.deltaY < 0;
  }

}
