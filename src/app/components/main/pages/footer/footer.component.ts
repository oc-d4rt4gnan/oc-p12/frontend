import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Doc } from 'src/app/models/document';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  @Input() document: Doc;
  @Input() isEdit: boolean;
  isQuotation: boolean;
  isInvoice: boolean;

  constructor(private router: Router) { 
    this.isQuotation = this.router.url.includes('/quotation');
    this.isInvoice = this.router.url.includes('/invoice');
  }

  ngOnInit(): void {
  }
  
  onChangeContent = (key: string, event: Event): void => {
    this.set(key, (event.target as HTMLElement).innerHTML);
  }

  private set = (_key: string, value: string): void => {
    switch(_key) {
      case 'validityMessage':
        this.document.validityMessage = value;
        break;
      case 'paymentMethod':
        this.document.paymentMethod = value;
        break;
      case 'conditions':
        this.document.conditions = value;
        break;
      case 'companyBankDetails':
        this.document.companyBankDetails = value;
        break;
      case 'companyInformation':
        this.document.companyInformation = value;
        break;
      default:
        break;
    }
  }

}
