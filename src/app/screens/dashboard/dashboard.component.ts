import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DocumentService } from 'src/app/services/document.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  values = [];
  lastPage: number;
  ready = false;

  constructor(private documentService: DocumentService) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.notifierTotalPageSubscription.unsubscribe();
  }

  notifierTotalPageSubscription: Subscription = this.documentService.subjectTotalPageNotifier.subscribe((totalPage: number) => {
    this.values = Array.from(Array(totalPage), (_,i)=> i+1);
    this.lastPage = this.values[this.values.length-1];
    this.ready = true;
  });

}
