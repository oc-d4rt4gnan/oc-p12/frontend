import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ContentLine } from 'src/app/models/content-line';
import { ArticlesService } from 'src/app/services/articles.service'
import { DocumentService } from 'src/app/services/document.service';

@Component({
  selector: 'app-article-selector',
  templateUrl: './article-selector.component.html',
  styleUrls: ['./article-selector.component.scss']
})
export class ArticleSelectorComponent implements OnInit {

  articles: ContentLine[];
  hasArticles: boolean;

  constructor(private articlesService: ArticlesService, private documentService: DocumentService) { 
    this.articlesService.getArticles().subscribe(data => { 
      this.articles = data; 
      this.hasArticles = this.articles.length > 0;
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.notifierSearchSubscription.unsubscribe();
  }

  selectArticle = (event: Event): void => {
    var articleName = (event.target as HTMLLIElement).innerHTML;
    var article: ContentLine = this.articles.find(article => article.designation == articleName);
    this.documentService.notifyAboutPushArticleInSelectedLine(article);
  } 

  notifierSearchSubscription: Subscription = this.articlesService.subjectSearchNotifier.subscribe((data: ContentLine[]) => {
    this.articles = data;
  });

}
