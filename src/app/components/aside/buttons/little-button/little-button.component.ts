import { AfterViewInit, Component, ElementRef, Input, ViewChild } from '@angular/core';

@Component({
  selector: 'app-little-button',
  templateUrl: './little-button.component.html',
  styleUrls: ['./little-button.component.scss']
})
export class LittleButtonComponent implements AfterViewInit {

  @Input() icon: string;

  @ViewChild('littleButton') button: ElementRef;

  constructor() { }

  ngAfterViewInit(): void {
    this.button.nativeElement.style.height = this.button.nativeElement.clientWidth + 'px';
  }

}
