import { ElementRef, Injectable } from '@angular/core';
import { jsPDF } from 'jspdf'
import html2canvas from 'html2canvas';
import { Observable, Subject } from 'rxjs';
import { Doc } from 'src/app/models/document';
import { Canvas } from 'src/app/models/canvas';
import { HttpClient } from '@angular/common/http';
import { ContentLine } from '../models/content-line';

@Injectable({
  providedIn: 'root'
})
export class DocumentsService {

  subjectDeletionNotifier: Subject<any> = new Subject<any>();
  subjectAddingNotifier: Subject<any> = new Subject<any>();
  subjectTotalUpdateNotifier: Subject<any> = new Subject<any>();
  subjectTotalUpdateBisNotifier: Subject<any> = new Subject<any>();
  subjectSelectionNotifier: Subject<any> = new Subject<any>();
  subjectPushArticleInSelectedLineNotifier: Subject<any> = new Subject<any>();

  hasPermission: boolean = true;

  constructor(private http: HttpClient) { }

  notifyAboutDeletion = (element: any): void => {
    this.subjectDeletionNotifier.next(element);
  }

  notifyAboutAdding = (element: any): void => {
    this.subjectAddingNotifier.next(element);
  }

  notifyAboutTotalUpdate = (): void => {
    this.subjectTotalUpdateNotifier.next();
  }

  notifyAboutTotalUpdateBis = (): void => {
    this.subjectTotalUpdateBisNotifier.next();
  }

  notifyAboutSelection = (): void => {
    this.subjectSelectionNotifier.next();
  }

  notifyAboutPushArticleInSelectedLine = (article: ContentLine): void => {
    this.subjectPushArticleInSelectedLineNotifier.next(article);
  }

  getDocumentDetails = (): Observable<Doc> => {
    return this.http.get<Doc>("assets/dataset/document.json");
  }

  getNewDocumentPattern = (): Observable<Document> => {
    let subject:Subject<Document> = new Subject();
    this.http.get<Document>("assets/dataset/document.json").subscribe(document => {
      document[0].clientAddress = '';
      document[0].clientName = '';
      document[0].discountExcludingTax = null;
      document[0].discountIncludingTax = null;
      document[0].netTotalExcludingTax = 0;
      document[0].reference = '';
      document[0].subjet = '';
      document[0].totalAmountExcludingTax = 0;
      document[0].totalAmountIncludingTax = 0;
      document[0].totalVAT = 0;
      document[0].type = null;
      document[0].content[0].content[0].designation = '';
      document[0].content[0].content[0].quantity = 0;
      document[0].content[0].content[0].unitPrice = 0;
      document[0].content[0].content[0].totalPrice = 0;
      document[0].content[0].content = [document[0].content[0].content[0]];
      document[0].content[0].name = '';
      document[0].content = [document[0].content[0]];
      subject.next(document[0]);
      subject.complete();
    });
    return subject;
  }

  save = (document: Document): Promise<Document> => {
    return new Promise((resolve, reject) => {
    
      /* Simulating some async stuff... */
      setTimeout(() => {
      
          if (this.hasPermission !== true) {
              reject(new Error('Permission denied!'));
              return;
          }
          
          resolve(document);
      
      }, 1000);
  
    });
  }

  downloadDocumentAsPDF = async (document: ElementRef, url: string) => {
    var PDF = await this.setPDF(document);
    PDF.save(this.constructPDFName(url));
  }

  printDocumentAsPDF = async (document: ElementRef) => {
    var PDF = await this.setPDF(document);
    PDF.autoPrint();
    window.open(PDF.output('bloburl'), '_blank');
  }

  private setPDF = async (document: ElementRef): Promise<jsPDF> => {
    const pages = document.nativeElement.children[0].children[0];
    let PDF = new jsPDF('p', 'mm', 'a4');
    var pageNumber = 1;
    const canvasList = await this.getCanvasList(pages);
    for (let canvas of canvasList) {
      if (pageNumber > 1) {
        PDF.addPage('a4', 'p');
        PDF.setPage(pageNumber);
      }
      PDF.addImage(canvas.uri, 'PNG', 0, 0, canvas.width, canvas.height);
      pageNumber++;
    }
    return PDF;
  }

  private constructPDFName = (url: string): string => {
    var urlArray = url.split('/');
    var reference = urlArray.pop().split('?')[0];
    var type = url.includes('/quotation') ? 'devis' : 'facture';
    return type + '_' + reference + '.pdf';
  }

  private getCanvasList = (pages: any): Promise<Canvas[]> => {
    return new Promise(async (resolve) => {
      var canvasArray: Canvas[] = new Array<Canvas>();
      for (let page of pages.children) {
        const canvas = await this.getCanvas(page);
        canvasArray.push(canvas);
      }
      resolve(canvasArray);
    });
  }

  private getCanvas = (page: any): Promise<Canvas> => {
    return new Promise((resolve) => {
      page.children[0].classList.add('print');
      html2canvas(page).then((canvas) => {
        let newCanvas = new Canvas();
        newCanvas.width = 210;
        newCanvas.height = (canvas.height * newCanvas.width) / canvas.width;
        newCanvas.uri = canvas.toDataURL('image/png', 100.0);
        page.children[0].classList.remove('print');
        resolve(newCanvas);
      });
    });
  }

}

