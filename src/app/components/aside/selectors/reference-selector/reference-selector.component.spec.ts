import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferenceSelectorComponent } from './reference-selector.component';

describe('ReferenceSelectorComponent', () => {
  let component: ReferenceSelectorComponent;
  let fixture: ComponentFixture<ReferenceSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferenceSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferenceSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
