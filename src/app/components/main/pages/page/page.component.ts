import { AfterViewInit, ChangeDetectorRef, Component, ComponentFactoryResolver, ComponentRef, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild, ViewContainerRef } from '@angular/core';
import { FooterComponent } from '../footer/footer.component';
import { HeaderComponent } from '../header/header.component';
import { Doc } from 'src/app/models/document';
import { TableComponent } from '../content-table/table/table.component';
import { Content } from 'src/app/models/content';
import { TotalComponent } from '../content-table/total/total.component';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit, AfterViewInit {

  @Input() document: Doc;
  @Input() data: any;
  isQuotation: boolean = true;
  isInvoice: boolean = false;
  @Input() pageNumber: number;
  @Input() isContentComplete: boolean;
  @Input() isEdit: boolean;
  @Output() endEvent = new EventEmitter<any>();
  @ViewChild('target', { static: false, read: ViewContainerRef }) marge: ViewContainerRef;

  private headerInstance: ComponentRef<HeaderComponent> = null;
  private contentInstance: ComponentRef<TableComponent> = null;
  private totalInstance: ComponentRef<TotalComponent> = null;
  private footerInstance: ComponentRef<FooterComponent> = null;

  constructor(
    private elRef: ElementRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    private cdr: ChangeDetectorRef
  ) { 
  }

  ngOnInit(): void {
    if (this.isContentComplete == undefined) {
      this.isContentComplete = this.data == undefined || (this.data instanceof Array && this.data.length == 0) || !(this.data instanceof Array);
    }
  }

  ngAfterViewInit(): void {
    var page: HTMLDivElement = this.elRef.nativeElement.children[0];
    page.style.width = page.clientHeight*(21/32.6)+'px';
    this.addHeader();
    var isFinish:boolean;
    if (this.isContentComplete == false) {
      isFinish = this.tryToAddContent(this.data);
    } 
    if (isFinish == true || this.isContentComplete) {
      isFinish = this.addTotal(isFinish);
      if (isFinish) {
        this.addFooter(isFinish);
      }
    }
  }

  isOverflowing = (): boolean => {
    const margeElement = this.marge.element.nativeElement;
    var curOverf = margeElement.style.overflow;
                  
    if ( !curOverf || curOverf === "visible" )
      margeElement.style.overflow = "hidden";
      
    var isOverflowing = margeElement.clientHeight < margeElement.scrollHeight;

    margeElement.style.overflow = curOverf;
      
    return isOverflowing;
  }

  addHeader = (): void => { 
    if (this.pageNumber == 1) { 
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(HeaderComponent);
      this.headerInstance = this.marge.createComponent(componentFactory);
      this.headerInstance.instance.document = this.document;
      this.headerInstance.instance.isEdit = this.isEdit;
      var componentElement = this.headerInstance.location.nativeElement;
      const sibling: HTMLElement = componentElement.previousSibling;
      sibling.appendChild(componentElement);
      this.cdr.detectChanges();
    }
  }

  addFooter = (isFinish: boolean): void => { 
    if (
      isFinish 
      || (
        !(this.data instanceof Array) 
        && (this.data as HTMLElement).tagName != undefined 
        && (this.data as HTMLElement).tagName.toLowerCase().includes('app-footer')
      )
    ) {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(FooterComponent);
      this.footerInstance = this.marge.createComponent(componentFactory);
      this.footerInstance.instance.document = this.document;
      this.footerInstance.instance.isEdit = this.isEdit;
      var componentElement = this.footerInstance.location.nativeElement;
      const sibling: HTMLElement = componentElement.previousSibling;
      sibling.appendChild(componentElement);
      this.cdr.detectChanges();
      if(this.isOverflowing()) {
        sibling.removeChild(componentElement);
        this.endEvent.emit(componentElement);
      }
    }
  }

  tryToAddContent = (content: Content[]): boolean => {
    var isFinish: boolean = true;

    // Create content component
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(TableComponent);
    this.contentInstance = this.marge.createComponent(componentFactory);
    this.contentInstance.instance.marge = this.marge.element.nativeElement

    // data injection
    this.contentInstance.instance.document = this.document
    this.contentInstance.instance.content = content; 
    this.contentInstance.instance.isEdit = this.isEdit;

    // Move new element into the correct tag
    var componentElement = this.contentInstance.location.nativeElement;
    const sibling: HTMLElement = componentElement.previousSibling;
    sibling.appendChild(componentElement);

    // check if overflowing event was emitted by children
    this.contentInstance.instance.needNewPageEvent.subscribe((data: any) => {
      this.endEvent.emit(data);
      isFinish = false;
    });
    this.cdr.detectChanges(); // Change detector to avoid error

    return isFinish;
  }

  addTotal = (isFinish: boolean): boolean => {
    if (
      isFinish 
      || (
        !(this.data instanceof Array) 
        && (this.data as HTMLElement).tagName != undefined 
        && (this.data as HTMLElement).tagName.toLowerCase().includes('app-total')
      )
    ) {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(TotalComponent);
      this.totalInstance = this.marge.createComponent(componentFactory);
      this.totalInstance.instance.document = this.document;
      var componentElement = this.totalInstance.location.nativeElement;
      const sibling: HTMLElement = componentElement.previousSibling;
      sibling.appendChild(componentElement);

      this.cdr.detectChanges();
      if (this.isOverflowing()) {
        sibling.removeChild(componentElement);
        this.endEvent.emit(componentElement);
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

}
