import { TestBed } from '@angular/core/testing';

import { DocumentMapperService } from './document-mapper.service';

describe('DocumentMapperService', () => {
  let service: DocumentMapperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DocumentMapperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
