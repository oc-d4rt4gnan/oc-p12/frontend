import { AfterViewInit, ChangeDetectorRef, Component, ComponentFactoryResolver, ComponentRef, Input, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Doc } from 'src/app/models/document';
import { PageComponent } from '../page/page.component';
import { Content } from 'src/app/models/content';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements AfterViewInit {

  private componentInstance: ComponentRef<PageComponent> = null;
  private lastPageNumber: number = 1;

  content: Content[];

  @Input() isEdit: boolean;
  @Input() document: Doc;
  @ViewChild("wrapper", { static: false, read: ViewContainerRef }) wrapper: ViewContainerRef;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private _changeDetectionRef: ChangeDetectorRef
  ) { }

  ngAfterViewInit(): void {
    this.content = this.document.content;
    this._changeDetectionRef.detectChanges();
  }

  addPage = (data: any) => {
    this.lastPageNumber++;
    this.createComponent(data);
    this.makeComponentAChild();
    this._changeDetectionRef.detectChanges();
  }

  private createComponent = (data: any) => {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(PageComponent);
    this.componentInstance = this.wrapper.createComponent(componentFactory);
    this.componentInstance.instance.pageNumber = this.lastPageNumber;
    if (data == undefined) {
      this.componentInstance.instance.isContentComplete = true;
    } else {
      this.componentInstance.instance.data = data;
    }
    this.componentInstance.instance.document = this.document;
    this.componentInstance.instance.isEdit = this.isEdit;
  }

  private makeComponentAChild(){
    var componentElement = this.componentInstance.location.nativeElement;
    const sibling: HTMLElement = componentElement.previousSibling;
    sibling.appendChild(componentElement);
    this.componentInstance.instance.endEvent.subscribe((data: any) => {
      this.addPage(data);
    });
  }

}
