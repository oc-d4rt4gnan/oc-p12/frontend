import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-circle-to-rhumbuses-spinner',
  templateUrl: './circle-to-rhumbuses-spinner.component.html',
  styleUrls: ['./circle-to-rhumbuses-spinner.component.scss']
})
export class CircleToRhumbusesSpinnerComponent implements OnInit {

  @Input() circleSize = 15;
  @Input() circlesNum = 3;
  @Input() animationDuration = 1200;

  constructor() { }

  ngOnInit(): void {
  }

  get circleMarginLeft() {
    return this.circleSize * 1.125;
  }

  get spinnerStyle() {
    return {
      height: `${this.circleSize}px`,
      width: `${(this.circleSize + this.circleMarginLeft) * this.circlesNum}px`
    };
  }

  get circleStyle() {
    return {
      animationDuration: `${this.animationDuration}ms`,
      height: `${this.circleSize}px`,
      width: `${this.circleSize}px`,
      marginLeft: `${this.circleMarginLeft}px`
    };
  }

  get circlesStyles() {
    const circlesStyles = [];
    const delay = 150;

    for (let i = 1; i <= this.circlesNum; i++) {
      const style = {
        ...this.circleStyle,
        animationDelay: `${i * delay}ms`
      };
      circlesStyles.push(style);
    }

    return circlesStyles;
  }


}
