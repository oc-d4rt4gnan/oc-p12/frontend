import { Content } from './content';

export class Doc {

    reference: string;
    type: string;
    companyLogoSVG: string;
    companyName: string;
    companyDescription: string;
    author: string;
    companyAddress: string;
    companyTelephone: string;
    companyEmail: string;
    clientName: string;
    clientAddress: string;
    creationDate: Date;
    updateDate: Date;
    creationLocation: string;
    subjet: string;
    content: Content[];
    totalAmountExcludingTax: number;
    discountExcludingTax: number;
    netTotalExcludingTax: number;
    totalVAT: number;
    discountIncludingTax: number;
    totalAmountIncludingTax: number;
    validityMessage: string;
    paymentMethod: string;
    conditions: string;
    companyBankDetails: string;
    companyInformation: string;
    status: string

}