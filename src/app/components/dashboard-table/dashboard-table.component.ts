import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-table',
  templateUrl: './dashboard-table.component.html',
  styleUrls: ['./dashboard-table.component.scss']
})
export class DashboardTableComponent implements OnInit {

  documents = [
    {
      reference: "201024814",
      client: {
        name: "Les clés du sud",
      },
      status: "PENDING",
      creationDate: "24/10/2020"
    },
    {
      reference: "201024814",
      client: {
        name: "Les clés du sud",
      },
      status: "PENDING",
      creationDate: "24/10/2020"
    },
    {
      reference: "201024814",
      client: {
        name: "Les clés du sud",
      },
      status: "PENDING",
      creationDate: "24/10/2020"
    },
    {
      reference: "201024814",
      client: {
        name: "Les clés du sud",
      },
      status: "PENDING",
      creationDate: "24/10/2020"
    },
    {
      reference: "201024814",
      client: {
        name: "Les clés du sud",
      },
      status: "PENDING",
      creationDate: "24/10/2020"
    },
  ];
  statusTitle: string;

  constructor(private router: Router) {
    switch (true) {
      case this.isUrlContains('/quotation'):
        this.statusTitle = "Accepté";
        break;
      case this.isUrlContains('/invoice'):
        this.statusTitle = "Payée";
        break;
      default:
        this.statusTitle = "";
      }
  }

  ngOnInit(): void {
  }

  isUrlContains = (path:string) : boolean => {
    return this.router.url.includes(path);
  }

}
