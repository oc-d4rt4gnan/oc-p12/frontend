export class Canvas {
    height: number;
    width: number;
    uri: string;
}