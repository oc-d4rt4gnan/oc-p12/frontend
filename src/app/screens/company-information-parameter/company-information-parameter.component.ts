import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ParametersService } from 'src/app/services/parameters.service';
import { CompanyInformation } from 'src/app/models/company-information';

@Component({
  selector: 'app-company-information-parameter',
  templateUrl: './company-information-parameter.component.html',
  styleUrls: ['./company-information-parameter.component.scss']
})
export class CompanyInformationParameterComponent implements OnInit {
  
  companyInformation: CompanyInformation;
  ready: boolean = false;
  showSpinner: boolean = false;

  constructor(private router: Router, private parametersService: ParametersService) { 
    this.parametersService.getCompanyInformation().subscribe(data => {
      this.companyInformation = data
      this.ready = true;
    });
  }

  ngOnInit(): void {
  }

  onChangeContent = (event: Event): void => {
    var td: HTMLTableCellElement = (event.target as HTMLTableCellElement);
    this.set(td.id, td.innerHTML);
  }

  onSave = (): void => {
    this.parametersService.saveCompanyInformation(this.companyInformation).toPromise()
    .then(result => {
      this.companyInformation = result;
      alert('Données enregistrées !')
    })
    .catch(() => {
      alert('Une erreur est survenue lors de l\'enregistrement !');
    });
  }

  private set = (_key: string, value: string): void => {
    switch(_key) {
      case 'logoSVG':
        this.companyInformation.logoSVG = value;
        break;
      case 'name':
        this.companyInformation.name = value;
        break;
      case 'description':
        this.companyInformation.description = value;
        break;
      case 'bankDetails':
        this.companyInformation.bankDetails = value;
        break;
      case 'otherInformation':
        this.companyInformation.otherInformation = value;
        break;
      default:
        break;
    }
  }

  private buildFileSelector = (): HTMLInputElement => {
    const fileSelector = document.createElement('input');
    fileSelector.setAttribute('type', 'file');
    fileSelector.setAttribute('multiple', 'multiple');
    return fileSelector;
  }

  uploadNewIcon = () => {
    var fileSelector = this.buildFileSelector();
    fileSelector.click();
    fileSelector.addEventListener('change', () => {
      if (fileSelector.files) {
        var tempLogo = this.companyInformation.logoSVG;
        if (fileSelector.files[0].type == "image/svg+xml") {
          fileSelector.files[0].text().then(value => this.companyInformation.logoSVG = value);
        } else {
          this.showSpinner = true;
          this.parametersService.convertImageToSVG(fileSelector.files[0]).then((res) => {
            this.companyInformation.logoSVG = res;
            this.showSpinner = false;
          }, 
          (err) => {
            console.log(err);
            this.companyInformation.logoSVG = tempLogo;
            this.showSpinner = false;
          });
        }
      }
    });
  }

}
