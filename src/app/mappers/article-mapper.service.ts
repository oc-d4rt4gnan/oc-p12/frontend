import { Injectable } from '@angular/core';
import { ContentLine } from 'src/app/models/content-line';
import { ApiArticle } from 'src/app/models/api-impl/api-article';
import { Article } from '../core/api/v1/model/article';

@Injectable({
  providedIn: 'root'
})
export class ArticleMapperService {

  constructor() { }

  toArticleArray = (contentLineArray: ContentLine[]): Article[] => {
    var articleArray: Article[] = new Array<Article>();
    contentLineArray.forEach(contentLine => {
      articleArray.push(this.toArticle(contentLine));
    });
    return articleArray;
  }

  private toArticle = (contentLine: ContentLine): Article => {
    var article: Article = new ApiArticle();
    article.designation = contentLine.designation;
    article.quantity = contentLine.quantity;
    article.unitPrice = contentLine.unitPrice;
    article.vat = contentLine.vat;
    return article;
  }

  toContentLineArray = (articleArray: Article[]): ContentLine[] => {
    var contentLineArray: ContentLine[] = new Array<ContentLine>();
    articleArray.forEach(article => {
      contentLineArray.push(this.toContentLine(article));
    });
    return contentLineArray;
  }

  private toContentLine = (article: Article): ContentLine => {
    var contentLine: ContentLine = new ContentLine();
    contentLine.designation = article.designation;
    contentLine.quantity = article.quantity;
    contentLine.unitPrice = article.unitPrice;
    contentLine.vat = article.vat;
    return contentLine;
  }
}
