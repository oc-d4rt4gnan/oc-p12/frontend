import { Component, Input } from '@angular/core';
import { Doc } from 'src/app/models/document';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

    @Input() document: Doc;
    @Input() isEdit: boolean;

    constructor() { }

    onChangeContent = (key: string, event: Event): void => {
        this.set(key, (event.target as HTMLElement).innerHTML);
    }

    private set = (_key: string, value: string): void => {
        switch(_key) {
            case 'author':
                this.document.author = value;
                break;
            case 'companyAddress':
                this.document.companyAddress = value;
                break;
            case 'companyTelephone':
                this.document.companyTelephone = value;
                break;
            case 'companyEmail':
                this.document.companyEmail = value;
                break;
            case 'clientName':
                this.document.clientName = value;
                break;
            case 'clientAddress':
                this.document.clientAddress = value;
                break;
            case 'creationLocation':
                this.document.creationLocation = value;
                break;
            case 'subjet':
                this.document.subjet = value;
                break;
            default:
                break;
        }
    }

}
