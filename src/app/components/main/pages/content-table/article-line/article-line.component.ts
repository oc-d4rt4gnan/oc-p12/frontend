import { ChangeDetectorRef, Component, ElementRef, HostListener, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ContentLine } from 'src/app/models/content-line';
import { DocumentService } from 'src/app/services/document.service';

@Component({
  selector: 'tr .article-line',
  templateUrl: './article-line.component.html',
  styleUrls: ['./article-line.component.scss']
})
export class ArticleLineComponent implements OnInit {

  @Input() article: ContentLine = null;
  @Input() isEdit: boolean;

  constructor(
    private elRef: ElementRef, 
    private documentService: DocumentService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.article.totalPrice = this.calculTotalPrice();
  }

  ngAfterViewInit(): void {
    this.article.checked = false;
  }

  addArticleLine = (): void => {
    this.documentService.notifyAboutAdding(this.article);
  }

  deleteArticleLine = (): void => {
    this.elRef.nativeElement.remove();
    this.documentService.notifyAboutDeletion(this.article);
  }

  onChangeContent = (_key: string, event: Event): void => {
    var value = (event.target as HTMLElement).innerHTML;
    switch(_key) {
      case 'designation':
        this.article.designation = value;
        break;
      case 'quantity':
        this.article.quantity = parseInt(value);
        this.article.totalPrice = this.calculTotalPrice();
        this.documentService.notifyAboutTotalUpdate();
        break;
      case 'unit-price':
        this.article.unitPrice = parseFloat(value);
        this.article.totalPrice = this.calculTotalPrice();
        this.documentService.notifyAboutTotalUpdate();
        break;
      default:
        break;
    }
    this.cdr.detectChanges();
  }

  private calculTotalPrice = (): number => {
    return this.article.unitPrice*this.article.quantity;
  }

  notifierPushArticleInSelectedLineSubscription: Subscription = this.documentService.subjectPushArticleInSelectedLineNotifier.subscribe((article: ContentLine) => {
    if (this.article.checked) {
      this.article.designation = {...article}.designation;
      this.article.quantity = {...article}.quantity;
      this.article.unitPrice = {...article}.unitPrice;
      this.article.totalPrice = this.calculTotalPrice();
      this.documentService.notifyAboutTotalUpdateBis();
    }
  });

  onCheck = (): void => {
    this.article.checked = true;
  }
}
