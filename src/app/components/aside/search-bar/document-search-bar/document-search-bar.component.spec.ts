import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentSearchBarComponent } from './document-search-bar.component';

describe('SearchBarComponent', () => {
  let component: DocumentSearchBarComponent;
  let fixture: ComponentFixture<DocumentSearchBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentSearchBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentSearchBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
