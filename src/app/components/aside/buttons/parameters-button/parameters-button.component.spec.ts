import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParametersButtonComponent } from './parameters-button.component';

describe('ParametersButtonComponent', () => {
  let component: ParametersButtonComponent;
  let fixture: ComponentFixture<ParametersButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParametersButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParametersButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
