import { TestBed } from '@angular/core/testing';

import { DocumentInformationMapperService } from './document-information-mapper.service';

describe('DocumentInformationMapperService', () => {
  let service: DocumentInformationMapperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DocumentInformationMapperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
