import { AfterViewInit, ChangeDetectorRef, Component, ComponentFactoryResolver, ComponentRef, Input, OnDestroy, ViewChild, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PagesComponent } from 'src/app/components/main/pages/pages/pages.component';
import { Type } from 'src/app/core/api/v1/model/type';
import { Content } from 'src/app/models/content';
import { ContentLine } from 'src/app/models/content-line';
import { Doc } from 'src/app/models/document';
import { DocumentService } from 'src/app/services/document.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements AfterViewInit, OnDestroy {

  reference: string;
  isEdit: boolean;
  document: Doc;
  tempDocument: Doc;
  cancelLink: string = "../../read/{{reference}}";

  private pagesInstance: ComponentRef<PagesComponent> = null;
  private onPrintAfterEdit: boolean = false;
  private dataFromNavigation: boolean = false;

  @ViewChild('pagesContainer', {static: false, read: ViewContainerRef}) pagesContainer: ViewContainerRef;

  constructor(
    private router: Router, 
    private documentService: DocumentService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private cdr: ChangeDetectorRef
  ) {
    this.reference = this.getReference(this.router);
    this.isEdit = this.isInEditionMode(this.router);
    this.onPrintAfterEdit = this.router.getCurrentNavigation().extras.state != undefined && this.router.getCurrentNavigation().extras.state.print != undefined;
    this.dataFromNavigation = this.getDocumentDataFromNavigation();
  }

  ngOnInit(): void {
    if (this.router.url.includes('new-document')) {
      this.documentService.getNewDocumentPattern(this.getType(this.router)).subscribe(data => {
        this.document = data;
        this.cancelLink = "../../";
      });
    } else if (!this.dataFromNavigation) {
      var reference = this.getReference(this.router);
      var type = this.getType(this.router);
      this.fetchData(reference, type);
    }
  }

  ngAfterViewInit(): void {
    if (this.onPrintAfterEdit) {
      this.onPrintPDF();
    }
  }

  ngOnDestroy(): void {
    this.notifierAddingSubscription.unsubscribe();
    this.notifierDeletionSubscription.unsubscribe();
    this.notifierTotalUpdateSubscription.unsubscribe();
  }

  private getReference = (router: Router): string => {
    var url: string[] = router.url.split('/');
    var lastElement: string = url.pop();
    var nullableReference:string = '';
    if (lastElement != undefined) {
      nullableReference = lastElement.split('?')[0];
    }
    return nullableReference;
  }

  private getType = (router: Router): Type => {
    switch (true) {
      case router.url.includes('/quotation'):
        return Type.Quotation;
      case router.url.includes('/invoice'):
        return Type.Invoice;
      default:
        return Type.Undefined;
    }
  }

  private isInEditionMode = (router: Router): boolean => {
    return router.url.includes('/edit');
  }

  notifierAddingSubscription: Subscription = this.documentService.subjectAddingNotifier.subscribe(element => {
    if (element.name != undefined) {
      var content: Content = this.deepClone(element);
      var article: ContentLine = this.createArticle();
      content.name = '';
      content.content = [article];
      this.document.content.splice(element.position + 1, 0, content);
    } else if (element.designation != undefined) {
      var article: ContentLine = this.createArticle();
      var sectionIndex: number;
      var articleIndex: number;
      this.document.content.forEach(section => {
        section.content.map((articleLine, i) => {
          if (articleLine.position == element.position) {
            sectionIndex = section.position;
            articleIndex = i;
          }
        })
      });
      this.document.content[sectionIndex].content.splice(articleIndex + 1, 0, article);
    }
    this.refreshPages(this.document);// originator has notified me. refresh my data here.
  });

  notifierDeletionSubscription: Subscription = this.documentService.subjectDeletionNotifier.subscribe(element => {
    if (element.name != undefined) {
      this.document.content.splice(element.position, 1);
    } else if (element.designation != undefined) {
      this.document.content = this.document.content.map(content => {
        content.content = content.content.filter(contentLine => contentLine.position != element.position);
        return content;
      });
    }
    this.refreshPages(this.document);// originator has notified me. refresh my data here.
  });

  notifierTotalUpdateSubscription: Subscription = this.documentService.subjectTotalUpdateNotifier.subscribe(() => {
    this.refreshPages(this.document);// originator has notified me. refresh my data here.
  });

  private refreshPages = (document: Doc): void => {
    this.document = this.setContentPosition(this.document);
    this.calculateTotal();
    var scrollTop = this.pagesContainer.element.nativeElement.children[0].children[0].scrollTop;
    this.pagesContainer.element.nativeElement.children[0].remove();
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(PagesComponent);
    this.pagesInstance = this.pagesContainer.createComponent(componentFactory);
    this.pagesInstance.instance.document = document;
    this.pagesInstance.instance.isEdit = this.isEdit;
    var componentElement = this.pagesInstance.location.nativeElement;
    const sibling: HTMLElement = componentElement.previousSibling;
    sibling.appendChild(componentElement);
    this.cdr.detectChanges();
    this.pagesContainer.element.nativeElement.children[0].children[0].scrollTop = scrollTop;
  }

  private setContentPosition = (document: Doc): Doc => {
    var indexLine = 0;
    document.content.map((content, i) => {
      content.position = i;
      content.content.map(contentLine => {
        contentLine.position = indexLine;
        indexLine++;
      });
    });
    return document;
  }

  private createArticle = (): ContentLine => {
    var articleLine: ContentLine = new ContentLine();
    articleLine.designation = '';
    articleLine.quantity = 0;
    articleLine.unitPrice = 0;
    articleLine.totalPrice = 0;
    return articleLine;
  }

  onSave = (): void => {
    if (this.isDocumentValid()) {
      this.document.type = this.getType(this.router);
      this.documentService.save(this.document).subscribe(document => {
        var urlArray = this.router.url.split('/');
        urlArray.splice(urlArray.length-2, 2);
        var url = urlArray.join('/');
        this.tempDocument = document;
        this.router.navigate([url + '/read/' + this.reference], { state: { tempDocument: this.tempDocument } });
        alert('Le document est bien enregistré !');
      });
    } else {
      alert('Le document n\'est pas complet. Veuillez compléter les champs manquant !');
    }
  }

  onDownloadPDF = (): void => {
    this.documentService.downloadDocumentAsPDF(this.pagesContainer.element, this.router.url);
  }

  onPrintPDF = (): void => {
    this.documentService.printDocumentAsPDF(this.pagesContainer.element);
  }

  onSaveAndPrintPDF = (): void => {
    if (this.isDocumentValid()) {
      this.documentService.save(this.document).subscribe(document => {
        var urlArray = this.router.url.split('/');
        urlArray.splice(urlArray.length-2, 2);
        var url = urlArray.join('/');
        this.tempDocument = document;
        this.router.navigate([url + '/read/' + this.reference], { state: { tempDocument: this.tempDocument, print: true } });
        alert('Le document est bien enregistré !');
      });
    } else {
      alert('Le document n\'est pas complet. Veuillez compléter les champs manquant !');
    }
  }

  fetchData = (reference: string, type: Type) => {
    return this.documentService.getDocumentDetails(reference, type).subscribe(data => {
      this.document = data;
      this.document = this.constructDocument(this.document);
    });
  }

  private constructDocument = (document: Doc): Doc => {
    document.content.map(section => section.displayName = true);
    document = this.setContentPosition(document);
    return document;
  }

  private getDocumentDataFromNavigation = (): boolean => {
    if (
      this.notInstanciate(this.router.getCurrentNavigation())
      && this.notInstanciate(this.router.getCurrentNavigation().extras)
      && this.notInstanciate(this.router.getCurrentNavigation().extras.state)
    ) { 
      var state = this.router.getCurrentNavigation().extras.state;

      if (
        this.router.url.includes('/read') 
        && this.notInstanciate(state.tempDocument)
      ) {
        this.document = this.deepClone(state.tempDocument);
      } else if (this.notInstanciate(state.document)) {
        this.document = this.deepClone(state.document);
        this.tempDocument = this.deepClone(state.document);
      } else {
        return false;
      }

      this.document = this.constructDocument(this.document);
    }

    return this.document != undefined && this.document != null;
  }

  private notInstanciate = (object: any): boolean => {
    return object != undefined && object != null;
  }

  private notEmpty = (object: string): boolean => {
    return this.notInstanciate(object) && object != '';
  }

  private deepClone = (object: any): any => {
    return JSON.parse(JSON.stringify(object));
  }

  private calculateTotal = (): void => {
    this.document.totalAmountExcludingTax = this.document.content
      .map(section => section.content
        .map(line => line.totalPrice)
        .reduce((sum, current) => sum + current)
      )
      .reduce((sum, current) => sum + current);

    this.document.totalVAT = this.document.content
    .map(section => section.content
      .map(line => line.totalPrice*(line.vat = this.notInstanciate(line.vat) ? line.vat : 0.1))
      .reduce((sum, current) => sum + current)
    )
    .reduce((sum, current) => sum + current);

    this.document.totalAmountIncludingTax = this.document.totalAmountExcludingTax + this.document.totalVAT;
  }

  private isDocumentValid = (): boolean => {
    return this.notEmpty(this.document.clientAddress) && this.notEmpty(this.document.clientAddress) && this.notEmpty(this.document.subjet);
  }

}
