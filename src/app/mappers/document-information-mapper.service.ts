import { Injectable } from '@angular/core';
import { DocumentInformation } from '../core/api/v1/model/documentInformation';
import { ApiDocumentInformation } from '../models/api-impl/api-document-information';
import { LocalDocumentInformation } from "../models/local-document-information";

@Injectable({
  providedIn: 'root'
})
export class DocumentInformationMapperService {

  constructor() { }

  toApiDocumentInformation = (localDocumentInformation: LocalDocumentInformation): DocumentInformation => {
    var documentInformation: DocumentInformation = new ApiDocumentInformation();
    
    documentInformation.conditions = localDocumentInformation.conditions;
    documentInformation.paymentMethod = localDocumentInformation.paymentMethod;
    documentInformation.companyId = localDocumentInformation.companyId;
    documentInformation.validityMessage = localDocumentInformation.validityMessage;

    return documentInformation;
  }

  toLocalDocumentInformation = (apiDocumentInformation: DocumentInformation): LocalDocumentInformation => {
    var localDocumentInformation: LocalDocumentInformation = new LocalDocumentInformation();
    
    localDocumentInformation.conditions = apiDocumentInformation.conditions;
    localDocumentInformation.paymentMethod = apiDocumentInformation.paymentMethod;
    localDocumentInformation.companyId = apiDocumentInformation.companyId;
    localDocumentInformation.validityMessage = apiDocumentInformation.validityMessage;

    return localDocumentInformation;
  }
}
