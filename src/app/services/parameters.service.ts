import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PersonalInformation } from 'src/app/models/personal-information';
import { ApiDocumentInformation } from 'src/app/models/api-impl/api-document-information';
import { LocalDocumentInformation } from 'src/app/models/local-document-information';
import { CompanyInformation } from '../models/company-information';
import { CompaniesService } from '../core/api/v1/api/companies.service';
import { UtilsService } from '../core/api/v1/api/utils.service';
import { PersonalInformationMapperService } from "src/app/mappers/personal-information-mapper.service";
import { DocumentInformationMapperService } from "src/app/mappers/document-information-mapper.service";
import { CompanyInformationMapperService } from "src/app/mappers/company-information-mapper.service";
import { Company } from '../core/api/v1/model/company';
import { DocumentInformationService } from '../core/api/v1/api/documentInformation.service';

@Injectable({
  providedIn: 'root'
})
export class ParametersService {

  selectedFile: ImageSnippet;

  constructor(
    private http: HttpClient,
    private companiesService: CompaniesService, 
    private personalInformationMapper: PersonalInformationMapperService, 
    private documentInformationService: DocumentInformationService,
    private documentInformationMapper: DocumentInformationMapperService,
    private companyInformationMapper: CompanyInformationMapperService,
    private utilsService: UtilsService
  ) { }

  getPersonalInformation = (): Observable<PersonalInformation> => {
    let subjectPersonalInformation:Subject<PersonalInformation> = new Subject();
    this.companiesService.getCompanies().subscribe(companies => {
      var personalInformation: PersonalInformation;
      if (companies.length > 0) {
        personalInformation = this.personalInformationMapper.toPersonalInformation(companies[0]);
      } else {
        personalInformation = new PersonalInformation();
      }
      subjectPersonalInformation.next(personalInformation);
      subjectPersonalInformation.complete();
    });
    return subjectPersonalInformation;
  }

  savePersonalInformation = (personalInformation: PersonalInformation): Observable<PersonalInformation> => {
    let subjectPersonalInformation:Subject<PersonalInformation> = new Subject();
    var company: Company = this.personalInformationMapper.toCompany(personalInformation);
    this.companiesService.updateCompany(company.id, company).subscribe(company => {
      var personalInformation: PersonalInformation = this.personalInformationMapper.toPersonalInformation(company);
      subjectPersonalInformation.next(personalInformation);
      subjectPersonalInformation.complete();
    },
    () => alert('Une erreur est survenue lors de l\'enregistrement !'));
    return subjectPersonalInformation;
  }

  getCompanyInformation = (): Observable<CompanyInformation> => {
    let subjectCompanyInformation:Subject<CompanyInformation> = new Subject();
    this.companiesService.getCompanies().subscribe(companies => {
      var companyInformation: CompanyInformation;
      if (companies.length > 0) {
        companyInformation = this.companyInformationMapper.toCompanyInformation(companies[0]);
      } else {
        companyInformation = new CompanyInformation();
      }
      subjectCompanyInformation.next(companyInformation);
      subjectCompanyInformation.complete();
    });
    return subjectCompanyInformation;
  }

  saveCompanyInformation = (companyInformation: CompanyInformation): Observable<CompanyInformation> => {
    let subjectCompanyInformation:Subject<CompanyInformation> = new Subject();
    var company: Company = this.companyInformationMapper.toCompany(companyInformation);
    this.companiesService.updateCompany(company.id, company).subscribe(company => {
      var localCompanyInformation: CompanyInformation = this.companyInformationMapper.toCompanyInformation(company);
      subjectCompanyInformation.next(localCompanyInformation);
      subjectCompanyInformation.complete();
    },
    () => alert('Une erreur est survenue lors de l\'enregistrement !'));
    return subjectCompanyInformation;
  }

  convertImageToSVG = async (file: File): Promise<string> => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        var imgAsString: string = reader.result.toString();
        this.utilsService.convertIcon(imgAsString).subscribe((res) => resolve ( res.value ), (err) => reject ( new Error(err) ));
      };
    });
  }

  getDocumentInformation = (): Observable<LocalDocumentInformation> => {
    let subjectDocumentInformation:Subject<LocalDocumentInformation> = new Subject();
    this.documentInformationService.getDocumentInformation("a2a5bbc1-bc8c-4375-b7be-7b839daefdd8").subscribe(data => {
      var documentInformation: LocalDocumentInformation = this.documentInformationMapper.toLocalDocumentInformation(data);
      subjectDocumentInformation.next(documentInformation);
      subjectDocumentInformation.complete();
    });
    return subjectDocumentInformation;
  }

  saveDocumentInformation = (documentInformation: LocalDocumentInformation): Observable<LocalDocumentInformation> => {
    let subjectDocumentInformation:Subject<LocalDocumentInformation> = new Subject();
    var apiDocumentInformation: ApiDocumentInformation = this.documentInformationMapper.toApiDocumentInformation(documentInformation);
    this.documentInformationService.saveDocumentInformation(documentInformation.companyId, apiDocumentInformation).subscribe(data => {
      var localDocumentInformation: LocalDocumentInformation = this.documentInformationMapper.toLocalDocumentInformation(data);
      subjectDocumentInformation.next(localDocumentInformation);
      subjectDocumentInformation.complete();
    },
    () => alert('Une erreur est survenue lors de l\'enregistrement !'));
    return subjectDocumentInformation;
  }
}

class ImageSnippet {
  constructor(public src: string, public file: File) {}
}
