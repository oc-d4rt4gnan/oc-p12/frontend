import { Component, ElementRef, OnInit, Output } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RoutesRecognized } from '@angular/router';

@Component({
  selector: 'app-reference-selector',
  templateUrl: './reference-selector.component.html',
  styleUrls: ['./reference-selector.component.scss']
})
export class ReferenceSelectorComponent implements OnInit {

  references = [];
  wheel: any;

  constructor(private route: Router, private elRef: ElementRef) {
    this.references = Array.from(Array(40), (_,i)=> '2020105'+(i<9?'0':'')+(i+1));
  }

  ngOnInit(): void {
    this.wheel = this.elRef.nativeElement.querySelector('#wheel');
    this.route.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.ngAfterViewInit(); 
      }
    });
  }

  ngAfterViewInit(): void {
    var items = this.wheel.querySelector('ul').children;
    var urlArray = this.route.url.split('/');
    var selected = this.wheel.querySelector('.selected');
    if (selected != null) selected.classList.remove('selected');
    for (let item of items) {
      if (item.children[0].innerHTML === urlArray[urlArray.length-1]) {
        item.classList.add('selected');
        this.wheel.scrollTop = item.offsetTop - this.wheel.querySelector('ul').offsetTop;
        this.wheel.querySelector("#left").style.marginTop = this.wheel.scrollTop+"px";
        return;
      }
    }
  }

  onElementScroll = ($event: { deltaY: number; }) => {
    this.selectReferenceOnScroll($event);
    if (this.canScroll()) {
      this.wheel.querySelector("#left").style.marginTop = this.wheel.scrollTop+"px";
    }
  }

  canScroll = () : boolean => {
    const items = this.wheel.querySelector("ul");
    return this.wheel.scrollTop < items.offsetHeight;
  }

  selectReferenceOnScroll = ($event: { deltaY: number; }) => {
    const items = this.wheel.querySelector("ul");
    var currentItemSelected = items.querySelector('.selected');
    var urlArray = this.route.url.split('/');
    urlArray.pop();
    var url = urlArray.join('/');
    for (let i = 0; i < items.children.length; i++) {
      if (items.children[i] === currentItemSelected) {
        if (this.isScrollUp($event) && i > 0) {
          this.route.navigate([url+'/'+items.children[i-1].children[0].innerHTML]).then(() => {
            this.ngAfterViewInit();
          });
          return;
        } else if (!this.isScrollUp($event) && i < items.children.length-1) {
          this.route.navigate([url+'/'+items.children[i+1].children[0].innerHTML]).then(() => {
            this.ngAfterViewInit();
          });
          return;
        }
      }
    }
  }

  isScrollUp = ($event: { deltaY: number; }) => {
    return $event.deltaY < 0;
  }

}
