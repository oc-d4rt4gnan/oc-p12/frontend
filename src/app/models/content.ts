import { ContentLine } from "./content-line"

export class Content {
    displayName: boolean;
    position: number;
    name: string;
    content: ContentLine[];
}