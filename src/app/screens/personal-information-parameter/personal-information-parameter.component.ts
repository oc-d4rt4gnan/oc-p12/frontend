import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PersonalInformation } from 'src/app/models/personal-information';
import { ParametersService } from 'src/app/services/parameters.service';

@Component({
  selector: 'app-personal-information-parameter',
  templateUrl: './personal-information-parameter.component.html',
  styleUrls: ['./personal-information-parameter.component.scss']
})
export class PersonalInformationParameterComponent implements OnInit {

  personalInformation: PersonalInformation;
  ready: boolean = false;

  constructor(private router: Router, private parametersService: ParametersService) { 
    this.parametersService.getPersonalInformation().subscribe(data => {
      this.personalInformation = data;
      this.ready = true;
    });
  }

  ngOnInit(): void {
  }

  onChangeContent = (event: Event): void => {
    var td: HTMLTableCellElement = (event.target as HTMLTableCellElement);
    this.set(td.id, td.innerHTML);
  }

  onSave = (): void => {
    this.parametersService.savePersonalInformation(this.personalInformation).toPromise()
    .then(result => {
      this.personalInformation = result;
      alert('Données enregistrées !')
    })
    .catch(() => {
      alert('Une erreur est survenue lors de l\'enregistrement !');
    });
  }

  private includes = (pattern: string): boolean => {
    return this.router.url.includes(pattern);
  }

  private set = (_key: string, value: string): void => {
    switch(_key) {
        case 'lastName':
            this.personalInformation.lastName = value;
            break;
        case 'firstName':
            this.personalInformation.firstName = value;
            break;
        case 'address':
            this.personalInformation.address = value;
            break;
        case 'email':
            this.personalInformation.email = value;
            break;
        case 'phoneNumbre':
            this.personalInformation.phoneNumber = value;
            break;
        default:
            break;
    }
  }

}
