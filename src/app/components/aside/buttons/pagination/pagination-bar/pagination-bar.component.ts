import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DocumentService } from 'src/app/services/document.service';

@Component({
  selector: 'app-pagination-bar',
  templateUrl: './pagination-bar.component.html',
  styleUrls: ['./pagination-bar.component.scss']
})
export class PaginationBarComponent implements OnInit {

  @Input() values: Array<number>;
  previousPage: number;
  currentPage: number = 1;
  nextPage: number;
  @Input() lastPage: number;
  year: number;


  constructor(private route: ActivatedRoute, private elRef: ElementRef, private documentService: DocumentService) { }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        if (params != null && params.page != null && params.page != "") {
          this.currentPage = parseInt(params.page);
        }
        if (params != null && params.year != null && params.year != "") {
          this.year = parseInt(params.year);
        }
      }
    );

    if (this.isPreviousButtonsActive()) {
      this.previousPage = this.currentPage-1;
    }

    if (this.isNextButtonsActive()) {
      this.nextPage = this.currentPage+1;
    }     
  }

  ngAfterViewInit(): void {
    this.getCurrentButton().classList.add('active');
    this.manageSideButtons();
  }

  updatePaginationBar(page:number): void {
    this.documentService.notifyAboutPageChange(page, this.year);
    this.currentPage = page;
    this.previousPage = this.currentPage-1;
    this.nextPage = this.currentPage+1;
    if (this.elRef.nativeElement.querySelector('.active') != null) {
      this.elRef.nativeElement.querySelector('.active').classList.remove('active');
    }
    this.getCurrentButton().classList.add('active');
    this.manageSideButtons();
  }

  getCurrentButton = () : HTMLButtonElement => {
    var buttons = this.elRef.nativeElement.children[0].children;
    for (let button of buttons) {
      if (button.children[0].id === 'page'+this.currentPage) {
        return button.children[0];
      }    
    }    
  }

  isPreviousButtonsActive = () : boolean => {
    return this.currentPage > 1;
  }

  isNextButtonsActive = () : boolean => {
    return this.currentPage < this.lastPage;
  }

  manageSideButtons = () => {
    var firstPageButton: HTMLButtonElement = this.elRef.nativeElement.querySelector('#first-page-button').children[0];
    var previousPageButton: HTMLButtonElement = this.elRef.nativeElement.querySelector('#previous-page-button').children[0];
    var nextPageButton: HTMLButtonElement = this.elRef.nativeElement.querySelector('#next-page-button').children[0];
    var lastPageButton: HTMLButtonElement = this.elRef.nativeElement.querySelector('#last-page-button').children[0];
    
    if (this.isPreviousButtonsActive()) {
      firstPageButton.disabled = false;
      previousPageButton.disabled = false;
    } else {
      firstPageButton.disabled = true;
      previousPageButton.disabled = true;
    }

    if (this.isNextButtonsActive()) {
      nextPageButton.disabled = false;
      lastPageButton.disabled = false;
    } else {
      nextPageButton.disabled = true;
      lastPageButton.disabled = true;
    }
  }

}
