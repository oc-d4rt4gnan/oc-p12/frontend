import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateWheelComponent } from './date-wheel.component';

describe('DateWheelComponent', () => {
  let component: DateWheelComponent;
  let fixture: ComponentFixture<DateWheelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateWheelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateWheelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
