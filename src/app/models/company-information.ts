export class CompanyInformation {
    companyId: string;
    logoSVG: string;
    name: string;
    description: string;
    bankDetails: string;
    otherInformation: string;
}