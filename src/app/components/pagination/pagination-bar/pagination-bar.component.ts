import { Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pagination-bar',
  templateUrl: './pagination-bar.component.html',
  styleUrls: ['./pagination-bar.component.scss']
})
export class PaginationBarComponent implements OnInit {

  values = [];
  previousPage: number;
  currentPage: number = 1;
  nextPage: number;
  lastPage: number;

  constructor(private route: ActivatedRoute, private elRef: ElementRef) { 
    this.values = Array.from(Array(5), (_,i)=> i+1);
    this.lastPage = this.values[this.values.length-1];
  }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        if (params != null && params.page != null && params.page != "") {
          this.currentPage = parseInt(params.page);
        }
      }
    );

    if (this.isPreviousButtonsActive()) {
      this.previousPage = this.currentPage-1;
    }

    if (this.isNextButtonsActive()) {
      this.nextPage = this.currentPage+1;
    }     
  }

  ngAfterViewInit(): void {
    this.getCurrentButton().classList.add('active');
    this.manageSideButtons();
  }

  updatePaginationBar(page:number): void {
    this.currentPage = page;
    this.previousPage = this.currentPage-1;
    this.nextPage = this.currentPage+1;
    this.elRef.nativeElement.querySelector('.active').classList.remove('active');
    this.getCurrentButton().classList.add('active');
    this.manageSideButtons();
  }

  getCurrentButton = () : HTMLButtonElement => {
    var buttons = this.elRef.nativeElement.children[0].children;
    for (let button of buttons) {
      if (button.children[0].id === 'page'+this.currentPage) {
        return button.children[0];
      }    
    }    
  }

  isPreviousButtonsActive = () : boolean => {
    return this.currentPage > 1;
  }

  isNextButtonsActive = () : boolean => {
    return this.currentPage < this.lastPage;
  }

  manageSideButtons = () => {
    var firstPageButton: HTMLButtonElement = this.elRef.nativeElement.querySelector('#first-page-button').children[0];
    var previousPageButton: HTMLButtonElement = this.elRef.nativeElement.querySelector('#previous-page-button').children[0];
    var nextPageButton: HTMLButtonElement = this.elRef.nativeElement.querySelector('#next-page-button').children[0];
    var lastPageButton: HTMLButtonElement = this.elRef.nativeElement.querySelector('#last-page-button').children[0];
    
    if (this.isPreviousButtonsActive()) {
      firstPageButton.disabled = false;
      previousPageButton.disabled = false;
    } else {
      firstPageButton.disabled = true;
      previousPageButton.disabled = true;
    }

    if (this.isNextButtonsActive()) {
      nextPageButton.disabled = false;
      lastPageButton.disabled = false;
    } else {
      nextPageButton.disabled = true;
      lastPageButton.disabled = true;
    }
  }

}
