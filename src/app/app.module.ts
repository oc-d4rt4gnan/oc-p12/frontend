import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ApiModule } from './core/api/v1/api.module';
import { AppRoutingModule } from './app-routing.module';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr);

import { AppComponent } from './app.component';
import { HomeComponent } from './screens/home/home.component';
import { BigButtonComponent } from './components/aside/buttons/big-button/big-button.component';
import { ParametersButtonComponent } from './components/aside/buttons/parameters-button/parameters-button.component';
import { DashboardComponent } from './screens/dashboard/dashboard.component';
import { MediumButtonComponent } from './components/aside/buttons/medium-button/medium-button.component';
import { DocumentSearchBarComponent } from './components/aside/search-bar/document-search-bar/document-search-bar.component';
import { DateWheelComponent } from './components/aside/selectors/date-wheel/date-wheel.component';
import { DashboardTableComponent } from './components/main/dashboard-table/dashboard-table.component';
import { PaginationButtonComponent } from './components/aside/buttons/pagination/pagination-button/pagination-button.component';
import { PaginationBarComponent } from './components/aside/buttons/pagination/pagination-bar/pagination-bar.component';
import { DetailsComponent } from './screens/details/details.component';
import { ReferenceSelectorComponent } from './components/aside/selectors/reference-selector/reference-selector.component';
import { PageComponent } from './components/main/pages/page/page.component';
import { PagesComponent } from './components/main/pages/pages/pages.component';
import { TableComponent } from './components/main/pages/content-table/table/table.component';
import { SectionComponent } from './components/main/pages/content-table/section/section.component';
import { HeaderComponent } from './components/main/pages/header/header.component';
import { FooterComponent } from './components/main/pages/footer/footer.component';
import { ArticleLineComponent } from './components/main/pages/content-table/article-line/article-line.component';
import { TotalComponent } from './components/main/pages/content-table/total/total.component';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { LittleButtonComponent } from './components/aside/buttons/little-button/little-button.component';
import { ArticleSelectorComponent } from './components/aside/selectors/article-selector/article-selector.component';
import { ArticleSearchBarComponent } from './components/aside/search-bar/article-search-bar/article-search-bar.component';
import { ParametersComponent } from './screens/parameters/parameters.component';
import { ParameterButtonComponent } from './components/aside/buttons/parameter-button/parameter-button.component';
import { PersonalInformationParameterComponent } from './screens/personal-information-parameter/personal-information-parameter.component';
import { CompanyInformationParameterComponent } from './screens/company-information-parameter/company-information-parameter.component';
import { DocumentInformationParameterComponent } from './screens/document-information-parameter/document-information-parameter.component';
import { CircleToRhumbusesSpinnerComponent } from './components/spinners/circle-to-rhumbuses-spinner/circle-to-rhumbuses-spinner.component';

@NgModule({
  declarations: [
    // Components
    AppComponent,
    HomeComponent,
    BigButtonComponent,
    ParametersButtonComponent,
    DashboardComponent,
    MediumButtonComponent,
    DocumentSearchBarComponent,
    DateWheelComponent,
    DashboardTableComponent,
    PaginationButtonComponent,
    PaginationBarComponent,
    DetailsComponent,
    ReferenceSelectorComponent,
    PageComponent,
    PagesComponent,
    TableComponent,
    SectionComponent,
    HeaderComponent,
    FooterComponent,
    ArticleLineComponent,
    TotalComponent,
    LittleButtonComponent,
    ArticleSelectorComponent,
    ArticleSearchBarComponent,
    ParametersComponent,
    ParameterButtonComponent,
    PersonalInformationParameterComponent,

    // Pipes
    SafeHtmlPipe,
      CompanyInformationParameterComponent,
      DocumentInformationParameterComponent,
      CircleToRhumbusesSpinnerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ApiModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'fr_FR' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
