/**
 * OpenQIA Items MicroService
 * Microservice managing the items of the OpenQIA Project.
 *
 * The version of the OpenAPI document: 0.0.1-SNAPSHOT
 * Contact: sheepandhells@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface Item { 
    reference?: string;
    designation?: string;
    quantity?: number;
    unitPrice?: number;
    valueAddedTax?: number;
    totalPrice?: number;
}

