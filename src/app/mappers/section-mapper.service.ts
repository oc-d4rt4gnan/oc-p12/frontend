import { Injectable } from '@angular/core';
import { Content } from 'src/app/models/content';
import { ApiSection } from 'src/app/models/api-impl/api-section';
import { Section } from 'src/app/core/api/v1/model/section';
import { ArticleMapperService } from './article-mapper.service';

@Injectable({
  providedIn: 'root'
})
export class SectionMapperService {

  constructor(private articleMapper: ArticleMapperService) { }

  toSectionArray = (contentArray: Content[]): Section[] => {
    var sectionArray: Section[] = new Array<Section>();
    contentArray.forEach(content => {
      sectionArray.push(this.toSection(content));
    });
    return sectionArray;
  }

  private toSection = (content: Content): Section => {
    var section: Section = new ApiSection();
    section.name = content.name;
    section.content = this.articleMapper.toArticleArray(content.content);
    return section;
  }

  toContentArray = (sectionArray: Section[]): Content[] => {
    var contentArray: Content[] = new Array<Content>();
    sectionArray.forEach(section => {
      contentArray.push(this.toContent(section));
    });
    return contentArray;
  }

  private toContent = (section: Section): Content => {
    var content: Content = new Content();
    content.name = section.name;
    content.content = this.articleMapper.toContentLineArray(section.content);
    return content;
  }
}
