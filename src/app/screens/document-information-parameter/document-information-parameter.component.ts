import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalDocumentInformation } from 'src/app/models/local-document-information';
import { ParametersService } from 'src/app/services/parameters.service';

@Component({
  selector: 'app-document-information-parameter',
  templateUrl: './document-information-parameter.component.html',
  styleUrls: ['./document-information-parameter.component.scss']
})
export class DocumentInformationParameterComponent implements OnInit {

  documentInformation: LocalDocumentInformation;
  ready: boolean = false;

  constructor(private router: Router, private parametersService: ParametersService) { 
    this.parametersService.getDocumentInformation().subscribe(data => {
      this.documentInformation = data
      this.ready = true;
    });
  }

  ngOnInit(): void {
  }

  onChangeContent = (event: Event): void => {
    var td: HTMLTableCellElement = (event.target as HTMLTableCellElement);
    this.set(td.id, td.innerHTML);
  }

  onSave = (): void => {
    this.parametersService.saveDocumentInformation(this.documentInformation).toPromise()
    .then(result => {
      this.documentInformation = result;
      alert('Données enregistrées !')
    })
    .catch(() => {
      alert('Une erreur est survenue lors de l\'enregistrement !');
    });
  }

  private set = (_key: string, value: string): void => {
    switch(_key) {
      case 'validity':
        this.documentInformation.validityMessage = value;
        break;
      case 'paymentMethods':
        this.documentInformation.paymentMethod = value;
        break;
      case 'condition':
        this.documentInformation.conditions = value;
        break;
      default:
        break;
    }
  }

}
