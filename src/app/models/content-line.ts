export class ContentLine {
    position: number;
    designation: string;
    quantity: number;
    unitPrice: number;
    vat: number;
    totalPrice: number;
    checked: boolean;
}