import { AfterViewInit, ChangeDetectorRef, Component, ComponentFactoryResolver, ComponentRef, ElementRef, EventEmitter, Input, Output, ViewChild, ViewContainerRef } from '@angular/core';
import { Content } from 'src/app/models/content';
import { ContentLine } from 'src/app/models/content-line';
import { DocumentService } from 'src/app/services/document.service';
import { ArticleLineComponent } from '../article-line/article-line.component';

@Component({
  selector: 'tbody [app-table-section]',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss']
})
export class SectionComponent implements AfterViewInit {

  @Input() section: Content;
  @Input() marge: any;
  @Input() isEdit: boolean;
  @Output() endSectionEvent = new EventEmitter<ContentLine[]>();
  @ViewChild('articleLine', { static: false, read: ViewContainerRef }) articleLine: ViewContainerRef;

  private articleInstance: ComponentRef<ArticleLineComponent> = null;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private cdr: ChangeDetectorRef,
    private elRef: ElementRef,
    private documentService: DocumentService
  ) { }

  ngAfterViewInit(): void {
    var tempContent = [...this.section.content];
    for (let article of this.section.content) {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(ArticleLineComponent);
      this.articleInstance = this.articleLine.createComponent(componentFactory);
      this.articleInstance.instance.article = article;
      this.articleInstance.instance.isEdit = this.isEdit;
      this.cdr.detectChanges();
      if (this.isOverflowing()) {
        this.articleInstance.location.nativeElement.remove();
        this.endSectionEvent.emit(tempContent);
        this.cdr.detectChanges();
        return;
      } else {
        tempContent.shift();
      }
    }
  }

  private isOverflowing = (): boolean => {
    var curOverf = this.marge.style.overflow;
                  
    if ( !curOverf || curOverf === "visible" )
      this.marge.style.overflow = "hidden";
      
    var isOverflowing = this.marge.clientHeight < this.marge.scrollHeight;
      
    this.marge.style.overflow = curOverf;
      
    return isOverflowing;
  }

  addSection = () => {
    this.documentService.notifyAboutAdding(this.section);
  }

  deleteSection = () => {
    this.elRef.nativeElement.remove();
    this.documentService.notifyAboutDeletion(this.section);
  }

  onChangeContent = (event:Event): void => {
    this.section.name = (event.target as HTMLElement).innerHTML;
  }

}
