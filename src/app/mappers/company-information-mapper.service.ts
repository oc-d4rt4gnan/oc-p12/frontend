import { Injectable } from '@angular/core';
import { Company } from '../core/api/v1/model/company';
import { ApiCompany } from '../models/api-impl/api-company';
import{ CompanyInformation } from "../models/company-information";

@Injectable({
  providedIn: 'root'
})
export class CompanyInformationMapperService {
    
  constructor() { }

  toCompanyInformation(company: Company): CompanyInformation {
    var companyInformation: CompanyInformation = new CompanyInformation();
    companyInformation.companyId = company.id;
    companyInformation.logoSVG = company.logoSVG;
    companyInformation.name = company.name;
    companyInformation.description = company.description;
    companyInformation.otherInformation = company.otherInformation;
    companyInformation.bankDetails = company.bankDetail;
    return companyInformation;
  }

  toCompany(companyInformation: CompanyInformation): Company {
    var company: Company = new ApiCompany();
    company.id = companyInformation.companyId;
    company.logoSVG = companyInformation.logoSVG;
    company.name = companyInformation.name;
    company.description = companyInformation.description;
    company.otherInformation = companyInformation.otherInformation;
    company.bankDetail = companyInformation.bankDetails;
    return company;
  }

}
