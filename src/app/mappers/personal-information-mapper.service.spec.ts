import { TestBed } from '@angular/core/testing';

import { PersonalInformationMapperService } from './personal-information-mapper.service';

describe('PersonalInformationMapperService', () => {
  let service: PersonalInformationMapperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PersonalInformationMapperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
