import { Injectable } from '@angular/core';
import { Doc } from 'src/app/models/document';
import { ApiDocument } from 'src/app/models/api-impl/api-document';
import { Document } from 'src/app/core/api/v1/model/document';
import { Type } from 'src/app/core/api/v1/model/type';
import { Status } from 'src/app/core/api/v1/model/status';
import { SectionMapperService } from "src/app/mappers/section-mapper.service";
import { formatDate } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class DocumentMapperService {

  constructor(private sectionMapper: SectionMapperService) { }

  toApiDocument = (localDocument: Doc): Document => {
    var document: Document = new ApiDocument();
    document.author = localDocument.author;
    document.clientAddress = localDocument.clientAddress;
    document.clientName = localDocument.clientName;
    document.companyAddress = localDocument.companyAddress;
    document.companyBankDetails = localDocument.companyBankDetails;
    document.companyDescription = localDocument.companyDescription;
    document.companyEmail = localDocument.companyEmail;
    document.companyInformation = localDocument.companyInformation;
    document.companyLogoSVG = localDocument.companyLogoSVG;
    document.companyName = localDocument.companyName;
    document.companyTelephone = localDocument.companyTelephone;
    document.conditions = localDocument.conditions;
    document.content = this.sectionMapper.toSectionArray(localDocument.content);
    document.creationDate = formatDate(localDocument.creationDate, 'YYYY-MM-ddThh:mm:ssZ', 'fr-FR');
    document.creationLocation = localDocument.creationLocation;
    document.discountExcludingTax = localDocument.discountExcludingTax;
    document.discountIncludingTax = localDocument.discountIncludingTax;
    document.netTotalExcludingTax = localDocument.netTotalExcludingTax;
    document.paymentMethod = localDocument.paymentMethod;
    document.reference = localDocument.reference;
    document.status = this.toStatus(localDocument.status);
    document.subjet = localDocument.subjet;
    document.totalAmountExcludingTax = localDocument.totalAmountExcludingTax;
    document.totalAmountIncludingTax = localDocument.totalAmountIncludingTax;
    document.totalVAT = localDocument.totalVAT;
    document.type = this.toType(localDocument.type);
    document.updateDate = formatDate(localDocument.updateDate, 'YYYY-MM-ddThh:mm:ssZ', 'fr-FR');
    document.validityMessage = localDocument.validityMessage;

    return document;
  }

  toLocalDocument = (apiDocument: Document): Doc => {
    var document: Doc = new Doc();
    document.author = apiDocument.author;
    document.clientAddress = apiDocument.clientAddress;
    document.clientName = apiDocument.clientName;
    document.companyAddress = apiDocument.companyAddress;
    document.companyBankDetails = apiDocument.companyBankDetails;
    document.companyDescription = apiDocument.companyDescription;
    document.companyEmail = apiDocument.companyEmail;
    document.companyInformation = apiDocument.companyInformation;
    document.companyLogoSVG = apiDocument.companyLogoSVG;
    document.companyName = apiDocument.companyName;
    document.companyTelephone = apiDocument.companyTelephone;
    document.conditions = apiDocument.conditions;
    document.content = this.sectionMapper.toContentArray(apiDocument.content);
    document.creationDate = new Date(apiDocument.creationDate);
    document.creationLocation = apiDocument.creationLocation;
    document.discountExcludingTax = apiDocument.discountExcludingTax;
    document.discountIncludingTax = apiDocument.discountIncludingTax;
    document.netTotalExcludingTax = apiDocument.netTotalExcludingTax;
    document.paymentMethod = apiDocument.paymentMethod;
    document.reference = apiDocument.reference;
    document.status = apiDocument.status.valueOf();
    document.subjet = apiDocument.subjet;
    document.totalAmountExcludingTax = apiDocument.totalAmountExcludingTax;
    document.totalAmountIncludingTax = apiDocument.totalAmountIncludingTax;
    document.totalVAT = apiDocument.totalVAT;
    document.type = apiDocument.type.valueOf();
    document.updateDate = new Date(apiDocument.updateDate);
    document.validityMessage = apiDocument.validityMessage;

    return document;
  }

  toLocalDocuments = (apiDocumentArray: Document[]): Doc[] => {
    var documentArray: Doc[] = new Array<Doc>();
    apiDocumentArray.forEach(apiDocument => {
      documentArray.push(this.toLocalDocument(apiDocument));
    })
    return documentArray;
  }

  private toType = (type: string): Type => {
    switch(type) {
      case Type.Invoice.valueOf():
        return Type.Invoice;
      case Type.Quotation.valueOf():
        return Type.Quotation;
      case Type.Undefined.valueOf():
      default:
        return Type.Undefined;
    }
  }

  private toStatus = (status: string): Status => {
    switch(status) {
      case Status.Accepted.valueOf():
        return Status.Accepted;
      case Status.Paid.valueOf():
        return Status.Paid;
      case Status.Rejected.valueOf():
        return Status.Rejected;
      case Status.Pending.valueOf():
      default:
        return Status.Pending;
    }
  }
}
