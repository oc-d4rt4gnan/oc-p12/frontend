import { Component, OnInit } from '@angular/core';
import { ArticlesService } from 'src/app/services/articles.service';
import { ContentLine } from 'src/app/models/content-line';

@Component({
  selector: 'article-search-bar',
  templateUrl: './article-search-bar.component.html',
  styleUrls: ['./article-search-bar.component.scss']
})
export class ArticleSearchBarComponent implements OnInit {

  private articlesName: string[];

  constructor(private articlesService: ArticlesService) {
    var articles: ContentLine[] = [];
    this.articlesService.getArticles().toPromise().then(data => { 
      articles = data;
      this.articlesName = this.articlesService.getArticlesName(articles); 
    });
  }

  ngOnInit(): void {
  }

  onSearchChange(searchValue: string): void { 
    this.articlesService.getArticles(searchValue).toPromise().then(data => {
      this.articlesName = this.articlesService.getArticlesName(data);
      this.articlesService.notifyAboutSearch(data);
    });
  }

}
