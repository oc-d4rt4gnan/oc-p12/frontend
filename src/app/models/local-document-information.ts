export class LocalDocumentInformation {
    companyId: string;
    validityMessage: string;
    paymentMethod: string;
    conditions: string;
}