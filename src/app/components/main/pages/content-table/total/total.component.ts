import { AfterViewInit, Component, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { DocumentsService } from 'src/app/services/documents.service';
import { Doc } from 'src/app/models/document';

@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.scss']
})
export class TotalComponent implements AfterViewInit {

  @Input() document: Doc;

  hasDiscount: boolean;
  discountIsAfterTax: boolean;
  totalAmountExcludingTax: number;
  discountExcludingTax: number;
  netTotalExcludingTax: number;
  totalVAT: number;
  discountIncludingTax: number;
  totalAmountIncludingTax: number;

  constructor(private documentsService: DocumentsService) { }

  ngAfterViewInit(): void {
    this.fillTotalTable();
  }

  notifierTotalUpdateBisSubscription: Subscription = this.documentsService.subjectTotalUpdateBisNotifier.subscribe(() => {
    this.calculTotals();
    this.fillTotalTable();
  });

  private fillTotalTable = (): void => {
    this.hasDiscount = (this.document.discountExcludingTax != undefined && this.document.discountExcludingTax != null) || (this.document.discountIncludingTax != undefined && this.document.discountIncludingTax != null);
    if (this.hasDiscount) {
      this.discountIsAfterTax = this.document.discountIncludingTax != undefined && this.document.discountIncludingTax != null;
      if (this.discountIsAfterTax) {
        this.discountIncludingTax = this.document.discountIncludingTax;
      } else {
        this.discountExcludingTax = this.document.discountExcludingTax;
        this.netTotalExcludingTax = this.document.netTotalExcludingTax;
      }
    }
    this.totalAmountExcludingTax = this.document.totalAmountExcludingTax;
    this.totalVAT = this.document.totalVAT;
    this.totalAmountIncludingTax = this.document.totalAmountIncludingTax;
  }

  private calculTotals = (): void => {
    this.document.totalAmountExcludingTax = this.document.content
      .map(section => section.content
        .map(line => line.totalPrice)
        .reduce((sum, current) => sum + current)
      )
      .reduce((sum, current) => sum + current);

    this.document.totalVAT = this.document.content
    .map(section => section.content
      .map(line => line.totalPrice*(line.vat = this.notEmpty(line.vat) ? line.vat : 0.1))
      .reduce((sum, current) => sum + current)
    )
    .reduce((sum, current) => sum + current);

    this.document.totalAmountIncludingTax = this.document.totalAmountExcludingTax + this.document.totalVAT;
  }

  private notEmpty = (object: any): boolean => {
    return object != undefined && object != null;
  }

}
