import { Component, OnInit } from '@angular/core';
import { BigButtonComponent } from 'src/app/components/aside/buttons/big-button/big-button.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
